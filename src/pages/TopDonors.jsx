import React, { Component } from 'react'
import { Map, Toggler } from '../components'

export class TopDonors extends Component {
    state = {
        donors: null
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/events/topdonors`
        fetch(url)
        .then(res => res.json())
        .then(donors => {
            console.log(donors)
            this.setState({ donors })
        })
        .catch(err => console.log(err))
    }

    render() {
        const { donors } = this.state
        return (
            <div className='main-page'>
                Top Donors<br/>
                <hr/>
                {!donors && 'Loading...'}
                {donors && donors.map(donor => (
                    <div className='donor'>
                        Name: {donor.name}<br/>
                        Surname: {donor.surname}<br/>
                        Number of achievements: {donor.QUANTITY}<br/>
                        <hr/>
                    </div>
                    ))}
            </div>
        )
    }
}