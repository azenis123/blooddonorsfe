import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

import './profile.scss'

export class Profile extends Component {
    state = {
        name: null,
        surname: null,
        age: null,
        bloodType: null,
        factor: null,
        email: null,
        password: '',
        edit: false,
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/user`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(user => {
            console.log(user)
            const { name, surname, age, email } = user
            this.setState({ name, surname, age, email })
            // this.setState({ achievements })
        })
        .catch(err => console.log(err))
    }

    toggleEdit = () => {
        this.setState({ edit: !this.state.edit })
    }

    editProfile = () => {
        const { edit, password, ...rest } = this.state
        
        const changes = {
            ...rest
        }
        if (password.length > 0) changes[password] = password
        console.log(changes)
        const url = `http://localhost:3000/api/user/editinfo`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                body: JSON.stringify({changes})
            }
        )
        .then(res => res.json())
        .then(json => {
            if (json.status === 200) {
                this.setState({ edit: false })
            } else {
                alert('Something went wrong')
            }
            // this.setState({ achievements })
        })
        .catch(err => console.log(err))
    }
    
    handleChange = e => {
        const field = e.target.name
        const value = e.target.value
        this.setState({ 
            [field]: value
        })
    }

    render() {
        const { name, surname, age, bloodType, factor, email, edit, password } = this.state
        return (
            <>
                {!localStorage.getItem('jwt') && <Redirect to='/' />}
                <div className='profile'>
                    Your profile {!name && 'is loading...'}<br/>
                    <hr/>
                    {/* <form className="form" onChange={this.handleChange} onSubmit={this.editProfile}> */}
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleChange} disabled={!edit} type='text' name='name' value={name} />
                        <br/>
                        <label htmlFor="surname">Surname</label>
                        <input onChange={this.handleChange} disabled={!edit} type='text' name='surname' value={surname} />
                        <br/>
                        <label htmlFor="age">Age</label>
                        <input onChange={this.handleChange} disabled={!edit} type="number" name='age' min='1' max='99' value={age}/>
                        <br/>
                        <label htmlFor="bloodType">Blood Type</label>
                        <select disabled={!edit} name="bloodType" defaultValue={1}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                        <br/>
                        <label htmlFor="factor">Factor</label>
                        <select disabled={!edit} name="factor" defaultValue={'+'}>
                            <option value="+">+</option>
                            <option value="+">-</option>
                        </select>
                        <br/>
                        <label htmlFor="email">Email</label>
                        <input onChange={this.handleChange} disabled={!edit} type='text' name='email' type='email' value={email} />
                        <br/>
                        <label htmlFor="password">New password</label>
                        <input onChange={this.handleChange} disabled={!edit} type='password' name='password' value={password}/>
                        <br/>
                        {!edit && <button onClick={this.toggleEdit}>Change</button>}
                        {edit && (
                            <>
                                <button onClick={this.toggleEdit}>Cancel</button>
                                <button onClick={this.editProfile}>Upload changes</button>
                            </>
                        )}
                    {/* </form> */}
                    
                    {/* {achievements && achievements.length > 0 ?
                        achievements.map(achievement => (
                            <div>
                                Title: {achievement.title}<br/>
                                Date: {new Date(achievement.dateOfObrain).toString()}<br/>
                                Description: {achievement.description}
                                <hr/>
                            </div>
                        ))
                        :
                        'No achievements yet'
                    } */}
                </div>
            </>
        )
    }
}