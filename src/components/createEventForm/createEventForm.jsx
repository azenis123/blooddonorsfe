import React from 'react'

export class CreateEventForm extends React.Component {
    state = {
        title: '',
        description: '',
        realDate: '',
        address: '',
        lat: null,
        lon: null
    }

    handleChange = e => {
        const field = e.target.name
        const value = e.target.value
        this.setState({ 
            [field]: value
        })
    }

    upload = () => {
        if (!localStorage.jwt) return alert('You must log in before performing this operation')
        const url = `http://localhost:3000/api/events/create`
        const event = this.state
        console.log(event)
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: JSON.stringify({ event })
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)        
            this.props.addEvent(event)  
        })
        .catch(err => console.log(err))
    }


    render() {
        const { title, description, realDate, lat, lon, address } = this.state
        return (
            <>
                <form onChange={this.handleChange}>
                    <input placeholder='title' type="text" name='title' value={title} />
                    <input placeholder='description' type="text" name='description' value={description} />
                    <input placeholder='date' type="text" name='realDate' value={realDate} />
                    <input placeholder='lat' type="text" name='lat' value={lat} />
                    <input placeholder='lon' type="text" name='lon' value={lon} />
                    <input placeholder='address' type="text" name='address' value={address} />

                </form>
                <button onClick={this.props.cancelCreation}>Cancel</button>
                <button onClick={this.upload}>Upload</button>
            </>
        )
    }
} 