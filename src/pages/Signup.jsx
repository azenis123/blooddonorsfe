import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import './signup.scss'

const mockData = {
    name: 'Aleksandr',
    surname: 'Kobrin',
    age: '21',
    bloodType: 2,
    factor: '+',
    email: 'nontrip@yandex.ru',
    password: 'qwetyuio',
}

export class Signup extends Component {
    state = {
        name: mockData.name || '',
        surname: mockData.surname || '',
        age: mockData.age || '',
        bloodType: mockData.bloodType || 1,
        factor: mockData.factor || '+',
        email: mockData.email || '',
        password: mockData.password || '',
        authed: false
    }

    handleChange = e => {
        const field = e.target.name
        const value = e.target.value
        this.setState({ 
            [field]: value
        })
    }

    getSignup = () => {
        let {authed, ...res} = this.state
        const user = res
        console.log(user)
        const url = 'http://localhost:3000/auth/signup'
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                body: JSON.stringify(user)
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)
            alert('Thank you for registration! Please, go to Log In Page')
            // localStorage.setItem('jwt', json.accessToken)
            // this.props.changeAuthStatus('user')
            // this.setState({ authed: true })            
        })
        .catch(err => console.log(err))
    }

    submit = e => {
        e.preventDefault()
        // validate input
        console.log(this.state)

        this.getSignup()

        // setTimeout(() => {
        //     alert('Thank you! For account validation check your email box')
        //     this.setState({ authed: true })
        // }, 2000)
    }

    render() {
        const { name, surname, age, email, password, authed } = this.state
        return (
            authed ? <Redirect to='/' /> :
            <>
                <form onChange={this.handleChange} onSubmit={this.submit}>
                    <label htmlFor="name">Name</label>
                    <input type='text' name='name' value={name} />
                    <label htmlFor="surname">Surname</label>
                    <input type='text' name='surname' value={surname} />
                    <label htmlFor="age">Age</label>
                    <input type="number" name='age' min='1' max='99' value={age}/>
                    <label htmlFor="bloodType">Blood Type</label>
                    <select name="bloodType" defaultValue={1}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                    <label htmlFor="factor">Factor</label>
                    <select name="factor" defaultValue={'+'}>
                        <option value="+">+</option>
                        <option value="+">-</option>
                    </select>
                    <label htmlFor="email">Email</label>
                    <input type='text' name='email' type='email' value={email} />
                    <label htmlFor="password">Password</label>
                    <input type='password' name='password' value={password}/>
                    <input type='submit' value='Submit' />
                </form>

                <div>
                    Already have an account? <Link to='/login'>Log in</Link>
                </div>
            </>
        )
    }
}