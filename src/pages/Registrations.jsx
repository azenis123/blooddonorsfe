import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

export class Registrations extends Component {
    state = {
        registrations: null
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/registrations`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(registrations => {
            console.log(registrations)
            this.setState({ registrations })
        })
        .catch(err => console.log(err))
    }

    unsubscribe = (type, id) => {

        const url = type === 'Event' ? 
            `http://localhost:3000/api/events/unsubscribe?id=${id}` :
            `http://localhost:3000/api/hospitals/visitunreg?id=${id}`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: null
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)   
            if (json.status === 200) {
                console.log(this.state.registrations)
                let events = this.state.registrations.events
                let hospitals = this.state.registrations.hospitals
                if (type === 'event') {
                    events = events.filter(eventReg => {
                        return eventReg.sid !== id
                    })
                }
                if (type === 'hospital') {
                    hospitals = hospitals.filter(hospitalReg => {
                        return hospitalReg.hid !== id
                    })
                }
                this.setState({ 
                    registrations: {
                        events, hospitals
                    } 
                })
            }
        })
        .catch(err => {
            console.log(err)
            alert('Something went wrong')
        })
    }

    render() {
        const { registrations } = this.state
        return (
            <>
                {!localStorage.getItem('jwt') && <Redirect to='/' />}
                <div className='main-page'>
                    Your registrations<br/>
                    <hr/>
                    Events:<br/>
                    {registrations && registrations.events ?
                        registrations.events.map(registration => (
                            <div>
                                Title: {registration.title}<br/>
                                Date: {registration.realDate}<br/>
                                <Link to={`/event?id=${registration.eid}`}>More</Link>
                                <button onClick={() => this.unsubscribe('event', registration.sid)}>I won't go</button>
                                <hr/>
                            </div>
                        ))
                        :
                        'No active registration'
                    }
                    <hr/>
                    Hospital registrations:<br/>
                    {registrations && registrations.hospitals ?
                        registrations.hospitals.map(registration => (
                            <div>
                                Title: {registration.name}<br/>
                                Date: {registration.date}<br/>
                                <Link to={`/hospital?id=${registration.hid}`}>More</Link>
                                <button onClick={() => this.unsubscribe('hospital', registration.hid)}>I won't go</button>
                                <hr/>
                            </div>
                        ))
                        :
                        'No active registration'
                    }
                </div>
            </>
        )
    }
}