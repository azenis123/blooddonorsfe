import React, { Component } from 'react'
import { CreateEventForm } from '../components'
import { Link } from 'react-router-dom'
import './RegistrationsManagement.scss'

export class RegistrationsManagement extends Component {
    state = {
        registrations: null,
        bloodneed: {
            '1+': false,
            '1-': false,
            '2+': false,
            '2-': false,
            '3+': false,
            '3-': false,
            '4+': false,
            '4-': false,
        },
        creating: false
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/registrations/regshospital`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(registrations => {
            console.log(registrations)
            this.setState({ registrations })
        })
        .catch(err => console.log(err))
        
        fetch(`http://localhost:3000/api/hospitals/bloodneedmd`, 
        {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
                'authorization': `Bearer ${localStorage.jwt}`
            }
        }
        )
        .then(res => res.json())
        .then(bloodneed => {
            console.log(bloodneed)
            const bloodneedState = this.state.bloodneed
            bloodneed.forEach(blood => bloodneedState[blood.name] = true)
            console.log(bloodneedState)
            this.setState({ bloodneedState })
        })
        .catch(err => {
            console.log("BBBBBB")
            console.log(err)
        })
    }

    bloodNeedHandle = e => {
        const field = e.target.name
        const value = !this.state.bloodneed[field]
        this.setState({ 
            bloodneed: {
                ...this.state.bloodneed,
                [field]: value
            }
        })
    }

    confirmNeed = () => {
        fetch(`http://localhost:3000/api/hospitals/editbloodneed`, 
        {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
                'authorization': `Bearer ${localStorage.jwt}`
            },
            body: JSON.stringify({change: this.state.bloodneed })
        }
        )
        .then(res => res.json())
        .then(() => {
            alert('OK!')
        })  
        .catch(err => {
            alert('Something went wrong')
            console.log("BBBBBB")
            console.log(err)
        })
    }

    confirmVisit = (id, rid) => {
        const url = `http://localhost:3000/api/achievements/adddonation`
        console.log(rid)
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                body: JSON.stringify({ id, rid })
            }
        )
        .then(res => res.json())
        .then(json => {
            if (json.status !== 200) return alert('Something went wrong')
            const registrations = this.state.registrations.filter(registration => {
                return registration.id !== rid
            })
            this.setState({ registrations })
        })
        .catch(err => console.log(err))
    }

    cancelRegistration = id => {
        const url = `http://localhost:3000/api/registrations/unreghospital`

        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                body: JSON.stringify({ id })
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json.status)
        })
        .catch(err => console.log(err))
    }

    render() {
        const { events, creating, bloodneed, registrations } = this.state
        console.log(this.state)
        // const bloodtypes = ['1+', '1-', '2+', '2-', '3+', '3-', '4+', '4-']

        return (
            <div className='registrations-management'>
                Hospital Management<br/>
                <hr/>
                <div className="blood-need" style={{border: '1px solid black'}}>
                    <form onChange={this.bloodNeedHandle}>
                        {Object.keys(bloodneed).map(bloodtype => {
                            return (
                                <div className='input'>
                                    <label htmlFor={bloodtype}>{bloodtype}</label>
                                    <input type="checkbox" name={bloodtype} checked={bloodneed[bloodtype]}/>
                                </div>
                            )
                        })}
                    </form>
                        <button onClick={this.confirmNeed}>Confirm</button>
                </div>
                Registrations: <br/>
                {registrations ? 
                    registrations.map(registration => {
                        return <>
                            Name: {registration.name} {registration.surname}<br/>
                            Email: {registration.email}<br/>
                            Date: {registration.date}<br/>
                            <button onClick={() => this.confirmVisit(registration.did, registration.id)}>Confirm visit</button>
                            <button onClick={() => this.cancelRegistration(registration.id)}>Cancel registration</button>
                            <hr/>
                        </>
                    })
                    : 'Loading...'}
                <br/>
            </div>
        )
    }
}