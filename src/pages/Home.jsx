import React, { Component } from 'react'
import { Map, Toggler } from '../components'

const tabs = {
    Events: 'Events',
    Hospitals: 'Hospitals'
}

export class Home extends Component {
    state = {
        tab: tabs.Events,
        events: [],
        hospitals: []
    }

    changeTab = tab => {
        this.setState({ tab: tabs[tab] }, () => {
            if (this.state.tab === tabs.Hospitals && this.state.hospitals.length === 0) {
                const url = 'http://localhost:3000/api/hospitals'
                fetch(url)
                .then(res => res.json())
                .then(hospitals => {
                    console.log(hospitals)
                    this.setState({ hospitals }) 
                })
                .catch(err => console.log(err))
            }
        })
    }

    componentDidMount() {
        const url = 'http://localhost:3000/api/events'
        fetch(url)
        .then(res => res.json())
        .then(events => {
            console.log(events)
            this.setState({ events }) 
        })
        .catch(err => console.log(err))
    }

    render() {
        const { tab, events, hospitals } = this.state
        const markers = this.state.tab === tabs.Events ? events : hospitals
        return (
            <div className='main-page'>
                Home Page
                <Toggler 
                    points={Object.values(tabs)}
                    current={tab}
                    handler={this.changeTab}
                />
                <Map markers={markers} type={this.state.tab} />
            </div>
        )
    }
}