import React from 'react'
import './Toggler.scss'

export const Toggler = ({ points, handler, current }) => (
    <div className='toggler'>
        {points.map((point, i) => 
            (
                <div key={i} className={`toggler__point${current === point ? ' toggler__point_active' : ''}`} onClick={() => handler(point)}>
                    {point}
                </div>
            )
        )}
    </div>
)