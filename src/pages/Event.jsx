import React, { Component } from 'react'
import { Map, Toggler } from '../components'

export class Event extends Component {
    state = {
        event: null
    }

    componentDidMount() {
        const currentLocation = new URL(window.location.href)
        const id = currentLocation.searchParams.get('id')
        
        const url = `http://localhost:3000/api/events/event?id=${id}`
        fetch(url)
        .then(res => res.json())
        .then(event => {
            console.log(event)
            this.setState({ event })
        })
        .catch(err => console.log(err))
    }

    handleSubscription = () => {
        if (!localStorage.jwt) alert('You must log in before performing this operation')
        console.log(this.state.event)
        const url = `http://localhost:3000/api/events/subscribe?id=${this.state.event.id}`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: null
            }
        )
        .then(res => res.json())
        .then(json => {
            if (json.status === 200) return alert('Thank you! You can manage your subscriptions on "Registrations" page')
            alert('Something went wrong')
        })
        .catch(err => console.log(err))
    }

    render() {
        const { event } = this.state
        return (
            <div className='main-page'>
                Event<br/>
                {event ? 
                    <>
                        Title: {event.title}<br/>
                        Address: {event.address}<br/>
                        Date: {event.realDate}<br/>
                        Description: {event.description}<br/>
                        <button onClick={this.handleSubscription}>I will go</button>
                        <Map markers={[event]} type='Events'/> 
                    </> 
                    : 'Loading...'
                }
            </div>
        )
    }
}