import React, { Component } from 'react'
import { Map, Toggler } from '../components'
import { Redirect } from 'react-router-dom'

export class Logout extends Component {
    componentWillMount() {
        localStorage.removeItem('jwt')
        localStorage.removeItem('status')
        this.props.changeAuthStatus(null)
    }

    render() {
        return (
            <div>
                <Redirect to='/' />
            </div>
        )
    }
}