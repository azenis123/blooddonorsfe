import React, { Profiler } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { Header } from './components'
import {
  Home, 
  Login, 
  Signup, 
  Event, 
  Registrations, 
  TopDonors, 
  Logout, 
  Achievements,
  Hospital,
  Profile,
  Loginmd,
  EventsManagement,
  RegistrationsManagement
}  from './pages'

const auth = null

class App extends React.Component {
  state = {
    authStatus: localStorage.getItem('status') || null
  }

  changeAuthStatus = status => {
    console.log(status)
    this.setState({ authStatus: status })
  }

  render() {
    const { authStatus } = this.state
    return (
      <div className="App">
        <Router>
          <Header authStatus={authStatus} changeAuthStatus={this.changeAuthStatus}/>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/login">
              <Login changeAuthStatus={this.changeAuthStatus} />
            </Route>
            <Route path="/signup">
              <Signup />
            </Route>
            <Route path="/event">
              <Event />
            </Route>
            <Route path="/hospital">
              <Hospital />
            </Route>
            <Route path="/registrations">
              <Registrations />
            </Route>
            <Route path="/achievements">
              <Achievements />
            </Route>
            <Route path="/ranking">
              <TopDonors />
            </Route>
            <Route path="/profile">
              <Profile />
            </Route>
            <Route path="/loginmd">
              <Loginmd changeAuthStatus={this.changeAuthStatus}/>
            </Route>
            <Route path="/events-management">
              <EventsManagement changeAuthStatus={this.changeAuthStatus}/>
            </Route>
            <Route path="/hospital-management">
              <RegistrationsManagement changeAuthStatus={this.changeAuthStatus}/>
            </Route>
            <Route path="/logout">
              <Logout changeAuthStatus={this.changeAuthStatus}/>
            </Route>
          </Switch>
        </Router>
      </div>
    )
  };
}

export default App;
