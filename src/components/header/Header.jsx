import React from 'react'
import { Link } from 'react-router-dom'
import { menus } from './menus'
import './header.scss'

export const Header = ({ authStatus }) => (
    <div className='header'>
        <ul className='header__menu'>

            {!authStatus ? 
                renderMenuBullets(menus.default) : 
                renderMenuBullets(menus[authStatus])
            }
        
        </ul>
    </div>
)

const renderMenuBullets = bullets => bullets.map((bullet, i) => (
        <Link to={bullet.href} key={i}>
            <div className='header__bullet'>{bullet.name}</div>
        </Link>
    )
)
