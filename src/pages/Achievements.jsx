import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

export class Achievements extends Component {
    state = {
        achievements: null
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/achievements`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(achievements => {
            console.log(achievements)
            this.setState({ achievements })
        })
        .catch(err => console.log(err))
    }

    render() {
        const { achievements } = this.state
        return (
            <>
                {!localStorage.getItem('jwt') && <Redirect to='/' />}
                <div className='main-page'>
                    Your achievements<br/>
                    <hr/>
                    {achievements && achievements.length > 0 ?
                        achievements.map(achievement => (
                            <div>
                                Title: {achievement.title}<br/>
                                Date: {new Date(achievement.dateOfObrain).toString()}<br/>
                                Description: {achievement.description}
                                <hr/>
                            </div>
                        ))
                        :
                        'No achievements yet'
                    }
                </div>
            </>
        )
    }
}