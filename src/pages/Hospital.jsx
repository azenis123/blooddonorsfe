import React, { Component } from 'react'
import { Map, Toggler } from '../components'

export class Hospital extends Component {
    state = {
        hospital: null,
        date: '',
        bloodneed: null
    }

    componentDidMount() {
        const currentLocation = new URL(window.location.href)
        const id = currentLocation.searchParams.get('id')
        
        const url = `http://localhost:3000/api/hospitals/hospital?id=${id}`
        fetch(url)
        .then(res => res.json())
        .then(hospital => {
            console.log(hospital)
            this.setState({ hospital })
        })
        .catch(err => console.log(err))
        
        fetch(`http://localhost:3000/api/hospitals/bloodneed?id=${id}`, 
        {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
                'authorization': `Bearer ${localStorage.jwt}`
            }
        }
        )
        .then(res => res.json())
        .then(bloodneed => {
            console.log(bloodneed)
            this.setState({ bloodneed })
        })
        .catch(err => {
            console.log("BBBBBB")
            console.log(err)
        })
    }

    handleRegistration = () => {
        if (!localStorage.jwt) return alert('You must log in before performing this operation')
        const url = `http://localhost:3000/api/hospitals/visitreg?id=${this.state.hospital.id}`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: JSON.stringify({ date: this.state.date })
            }
        )
        .then(res => res.json())
        .then(json => {
            if (json.status !== 200) return alert('Something went wrong')
            alert('Thank you for registration!')
            this.setState({ date: '' })
        })
        .catch(err => console.log(err))
    }

    handleDate = e => {
        const date = e.target.value
        console.log(date)
        this.setState({ date })
    }

    render() {
        const { hospital, date, bloodneed } = this.state
        return (
            <div className='main-page'>
                Hospital<br/>
                {hospital ? 
                    <>
                        Name: {hospital.name}<br/>
                        Address: {hospital.address}<br/>
                        Phone: {hospital.phone}<br/>
                        <hr/>
                        Registration: <br/>
                        <input type="date" value={date} onChange={this.handleDate} /><br/>
                        <button onClick={this.handleRegistration}>Register for visit</button>
                        <hr/>
                        Blood types in need:
                        <ul>
                            {bloodneed && bloodneed.map(blood => <li>{blood.name}</li>)}
                        </ul>
                        <Map markers={[hospital]} /> 
                    </> 
                    : 'Loading...'
                }
            </div>
        )
    }
}