import { Home } from './Home'
import { Login } from './Login'
import { Signup } from './Signup'
import { Event } from './Event'
import { Registrations } from './Registrations'
import { TopDonors } from './TopDonors'
import { Logout } from './Logout'
import { Achievements } from './Achievements'
import { Hospital } from './Hospital'
import { Profile } from './Profile'
import { Loginmd } from './Loginmd'
import { EventsManagement } from './EventsManagement'
import { RegistrationsManagement } from './RegistrationsManagement'

export { 
    Home, 
    Login,
    Signup,
    Event,
    Registrations,
    TopDonors,
    Logout,
    Achievements,
    Hospital,
    Profile,
    Loginmd,
    EventsManagement,
    RegistrationsManagement
}