export const menus = {
    default: [
        {
            name: 'Home',
            href: '/'
        }, 
        {
            name: 'Log in',
            href: '/login'
        }, 
        {
            name: 'Log in as medical worker',
            href: '/loginmd'
        }, 
        {
            name: 'Sign up',
            href: '/signup'
        }
    ],
    user: [
        {
            name: 'Home',
            href: '/'
        },
        {
            name: 'Top donors',
            href: '/ranking'
        },
        {
            name: 'Profile',
            href: '/profile'
        },
        {
            name: 'Registrations',
            href: '/registrations'
        },
        {
            name: 'Achievements',
            href: '/achievements'
        },
        {
            name: 'Log out',
            href: '/logout'
        }
    ],
    doctor: [
        {
            name: 'Home',
            href: '/'
        },
        {
            name: 'Top donors',
            href: '/ranking'
        },
        // {
        //     name: 'Profile',
        //     href: '/profile'
        // },
        {
            name: 'Registrations',
            href: '/registrations'
        },
        {
            name: 'Log out',
            href: '/logout'
        },
        {
            name: 'Manage events',
            href: '/events-management'
        },
        {
            name: 'Manage hospital',
            href: '/hospital-management'
        }
    ],
    admin: [
        {
            name: 'Home',
            href: '/'
        },
        {
            name: 'Top donors',
            href: '/ranking'
        },
        {
            name: 'Profile',
            href: '/profile'
        },
        {
            name: 'Registrations',
            href: '/registrations'
        },
        {
            name: 'Log out',
            href: '/logout'
        }
    ]
}
