import React from 'react'
import { Map as MapLeaflet, Marker, Popup, TileLayer } from 'react-leaflet'
import './map.scss'
import { Link } from 'react-router-dom'

const position = [51.505, -0.09]
export const Map = ({ type, markers }) => (
    <MapLeaflet center={markers.length === 1 ? [markers[0].lat, markers[0].lon] : position} zoom={7}>
        <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution=""
        />
        {markers.map((marker, i) => {
            return <CustomPopup type={type} marker={marker} index={i} length={markers.length} />
        }
        )}
        {/* <Marker position={position}>
            <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
        </Marker> */}
    </MapLeaflet>
)

const CustomPopup = ({ type, marker, index, length }) => {
    const position = [marker.lat, marker.lon]
    console.log(type)
    if (type === 'Events') {
        return (
            <Marker position={position} key={index}>
                <Popup>
                    {marker.title}
                    {length > 1 && <Link to={`/event?id=${marker.id}`}>
                        <button>More</button>
                    </Link>}
                </Popup>
            </Marker>
        )
    } else {
        console.log('123')
        return (
            <Marker position={position} key={index}>
                <Popup>
                    {marker.name}
                    <Link to={`/hospital?id=${marker.id}`}>
                        <button>More</button>
                    </Link>
                </Popup>
            </Marker>
        )
    }
}
