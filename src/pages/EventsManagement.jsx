import React, { Component } from 'react'
import { CreateEventForm } from '../components'
import { Link } from 'react-router-dom'

export class EventsManagement extends Component {
    state = {
        events: null,
        creating: false
    }

    componentDidMount() {
        const url = `http://localhost:3000/api/events/eventsbymd`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(events => {
            console.log(events)
            this.setState({ events })
        })
        .catch(err => console.log(err))
    }

    addEvent = event => {
        const url = `http://localhost:3000/api/events/eventsbymd`

        fetch(url, 
            {
                method: 'GET',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                }
            }
        )
        .then(res => res.json())
        .then(events => {
            console.log(events)
            this.setState({
                events,
                creating: false
            })
        })
        .catch(err => console.log(err))
    }

    handleSubscription = () => {
        if (!localStorage.jwt) alert('You must log in before performing this operation')
        console.log(this.state.event)
        const url = `http://localhost:3000/api/events/subscribe?id=${this.state.event.id}`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: null
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)          
        })
        .catch(err => console.log(err))
    }

    cancelEvent = id => {
        const url = `http://localhost:3000/api/events/cancel?id=${id}`
        fetch(url, 
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.jwt}`
                },
                referrerPolicy: 'no-referrer',
                body: null
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)          
        })
        .catch(err => console.log(err))
    }

    toggleCreation = () => {
        this.setState({ creating: !this.state.creating })
    }

    render() {
        const { events, creating } = this.state
        return (
            <div className='main-page'>
                Events<br/>
                <hr/>
                {events ? 
                    events.length === 0 ? 'No events' :
                    events.map(event => (
                        <>
                            Title: {event.title}<br/>
                            Date: {event.realDate}<br/>
                            <button onClick={() => this.cancelEvent(event.id)}>Cancel</button>
                            <Link to={`/event?id=${event.id}`}>
                                <button>More</button>
                            </Link>
                            <br/>
                            <hr/>
                            {/* <Map markers={[event]} />  */}
                        </> 
                    ))
                    : 'Loading...'
                }
                <br/>
                <button onClick={this.toggleCreation}>Create event</button>
                {
                    creating && <CreateEventForm addEvent={this.addEvent} cancelCreation={this.toggleCreation}/>
                }
            </div>
        )
    }
}