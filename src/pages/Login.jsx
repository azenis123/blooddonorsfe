import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'

export class Login extends Component {
    state = {
        email: 'test@test.ru',
        password: '',
        authed: false
    }

    handleChange = e => {
        const field = e.target.name
        const value = e.target.value
        this.setState({ 
            [field]: value
        })
    }

    getAuth = () => {
        const user = {
            email: this.state.email,
            password: this.state.password
        }
        const url = 'http://localhost:3000/auth/login'
        fetch(url,
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                body: JSON.stringify(user)
            }
        )
        .then(res => res.json())
        .then(json => {
            console.log(json)
            if (json.status !== 200) return
            localStorage.setItem('jwt', json.accessToken)
            localStorage.setItem('status', 'user')
            console.log(localStorage.getItem('jwt'))
            this.props.changeAuthStatus('user')
            this.setState({ authed: true })        
        })
        .catch(err => console.log(err))
    }

    submit = e => {
        e.preventDefault()
        console.log(this.state)
        this.getAuth()
        // setTimeout(() => {
        //     console.log(1)
        //     const mockReply = 'user'
        //     if (mockReply === 'user') {
        //         console.log('AUTHED')
        //         sessionStorage.setItem('auth', 'user')
        //         this.props.changeAuthStatus('user')
        //         this.setState({ authed: true })
        //     }            
        // }, 2000)
    }

    render() {
        const { authed, email, password } = this.state
        return (
            authed ? <Redirect to='/' /> :
            <>
                LOG IN
                <form onChange={this.handleChange} onSubmit={this.submit}>
                    <input type='text' name='email' type='email' value={email} />
                    <input type='password' name='password' value={password}/>
                    <input type='submit' value='Submit' />
                </form>

                <div>
                    Don't have an account yet? <Link to='/signup'>Sign up</Link>
                </div>
            </>
        )
    }
}